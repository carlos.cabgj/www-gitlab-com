(function() {
  let mobileNavIsActive = false;
  const ul = document.getElementById("navigation-dropdown");
  const div = document.getElementById("navigation-mobile");
  const cta = document.getElementById("navigation-dropdown-cta");
  const menuButton = document.getElementById("slpMobileNavActive");
  
  //Event handler for clicking hamburger menu
menuButton.onclick = () => {
    if(!mobileNavIsActive){
      menuButton.firstElementChild.setAttribute("src", "/images/icons/slp-close.svg")
      ul.classList.add("navigation-dropdown--active");
      div.classList.add("navigation-mobile--active")
      cta.classList.add("navigation-dropdown-cta--active")
    }
    else {
      menuButton.firstElementChild.setAttribute("src", "/images/icons/slp-hamburger.svg")
      ul.classList.remove("navigation-dropdown--active");
      div.classList.remove("navigation-mobile--active")
      cta.classList.remove("navigation-dropdown-cta--active")
    }
    mobileNavIsActive = !mobileNavIsActive
  }

  //Event Handler for Mobile Drop Downs
  const buttons = Array.from(document.querySelectorAll("#navigation-dropdown__heading"));
  const uls = buttons.map(element => element.nextElementSibling)

  let dropdownThatAreOpen = () => uls.filter(element => element.classList.contains('navigation-dropdown-item--active'));

  buttons.forEach(element => element.onclick = () => {
    let current;
    if((current = dropdownThatAreOpen()).length === 1 && element.nextElementSibling !== current[0]) {
      current[0].classList.remove("navigation-dropdown-item--active");
      current[0].previousElementSibling.lastElementChild.lastElementChild.style.transform = 'rotate(0deg)'
    }
    
    if(element.nextElementSibling.classList.contains('navigation-dropdown-item--active')) {
      element.nextElementSibling.classList.remove("navigation-dropdown-item--active");
      element.lastElementChild.lastElementChild.style.transform = "rotate(0deg)"
    }
    else {
      element.nextElementSibling.classList.add("navigation-dropdown-item--active");
      element.lastElementChild.lastElementChild.style.transform = "rotate(180deg)"
    }

  });

})();
